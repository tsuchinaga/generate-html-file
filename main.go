package main

import (
	"html/template"
	"log"
	"os"
)

func main() {
	// テンプレートファイルのオープン
	t, err := template.ParseFiles("templates/index.gohtml")
	if err != nil {
		log.Println(err)
		return
	}

	// 出力ファイルのオープン
	file, err := os.Create("outputs/index.html")
	if err != nil {
		log.Println(err)
		return
	}
	defer file.Close()

	// テンプレートの結果を出力ファイルに書き込み
	err = t.Execute(file, map[string]string{"msg": "hello world!!!"})
	if err != nil {
		log.Println(err)
		return
	}
}
